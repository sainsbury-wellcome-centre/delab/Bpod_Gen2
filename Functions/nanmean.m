function [out] = nanmean(in)
    out = mean(in,'omitnan');
end